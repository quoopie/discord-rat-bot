package com.discord.bots;

import org.javacord.api.BotInviteBuilder;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.permission.PermissionType;
import org.javacord.api.entity.permission.PermissionsBuilder;

import java.util.Collection;

/**
 * A class made to help speed up certain bot actions
 */
public class botHelper {

    public botHelper() {}

    /**
     * @param message Discord message of user you want to ping
     * @return String which will ping message author when sent
     */
    public static String messageAuthorPing(Message message) {
        if (message.getUserAuthor().isEmpty()) { throw new NullPointerException("Unreachable"); }
        return message.getUserAuthor().get().getMentionTag();
    }

    /**
     * @param clientID Discord Application ID
     * @return A bot invite link that requires no set permissions
     */
    public static String botInviteMaker(String clientID) {
        BotInviteBuilder botInviteBuilder = new BotInviteBuilder(clientID);
        PermissionsBuilder permissionsBuilder = new PermissionsBuilder();

        permissionsBuilder.setAllDenied();
        botInviteBuilder.setPermissions(permissionsBuilder.build());
        return botInviteBuilder.build();
    }

    /**
     * @param clientID Discord Application ID
     * @param permissions Array of PermissionTypes that the bot requires in servers
     * @return A bot invite link with the set permissions
     */
    public static String botInviteMaker(String clientID, Collection<PermissionType> permissions) {
        BotInviteBuilder botInviteBuilder = new BotInviteBuilder(clientID);
        PermissionsBuilder permissionsBuilder = new PermissionsBuilder();

        for (PermissionType permission : permissions) {
            permissionsBuilder.setAllowed(permission);
        }

        botInviteBuilder.setPermissions(permissionsBuilder.build());
        return botInviteBuilder.build();
    }

    /**
     * @param clientID Discord Application ID
     * @return A bot invite link with all permissions required in servers
     */
    public static String botAdminInviteMaker(String clientID) {
        BotInviteBuilder botInviteBuilder = new BotInviteBuilder(clientID);
        PermissionsBuilder permissionsBuilder = new PermissionsBuilder();

        permissionsBuilder.setAllAllowed();
        botInviteBuilder.setPermissions(permissionsBuilder.build());
        return botInviteBuilder.build();
    }
}
