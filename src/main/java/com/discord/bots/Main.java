package com.discord.bots;

import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Tag;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.activity.ActivityType;

import java.awt.*;
import java.io.*;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        @SuppressWarnings("SpellCheckingInspection")
        String discordToken = "OTM2NzcxMDc2Mjg4MDQxMDAx.YfSCDA.kSHCPUO29sVAtBDmm1jwbF_bOVw";
        @SuppressWarnings("SpellCheckingInspection")
        String gitlabToken = "glpat-ZkNFPHh1eVrFWkzww1qs";

        SingletonManager singletonManager = SingletonManager.getInstance();

        URL releaseURL = null;

        try {
            releaseURL = new URL("https://gitlab.com/quoopie/discord-rat-bot/-/releases");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Logger logger = singletonManager.getLogger();

        String version = "v2.4.11";

        boolean isLatestMajorVer = true;

        GitLabApi gitlabApi = new GitLabApi("https://gitlab.com/", gitlabToken);

        try {
            assert (releaseURL != null);
            List<Tag> tags = gitlabApi.getTagsApi().getTags(33236080);
            String latestVer = tags.get(0).getName();
            if (!version.split("\\.")[0].equals(latestVer.split("\\.")[0])) {
                logger.severe("There is a major update available. You must update before the bot will run!");
                logger.info("You are on release `" + version +"`. The latest release is " + latestVer);
                logger.info("Attempting to open releases page");
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                assert (desktop != null);
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    desktop.browse(releaseURL.toURI());
                    logger.info("Release page opened");
                } else {
                    logger.severe("Unable to open link. Please manually open it at " +
                            releaseURL);
                }
                isLatestMajorVer = false;
            } else if (!version.split("\\.")[1].equals(latestVer.split("\\.")[1])) {
                logger.severe("There is an update available at " +
                        releaseURL +"\n" + "The bot will still run but it is recommended to update!");
                logger.info("You are on release `" + version +"`. The latest release is " + latestVer);
            } else if (latestVer.split("\\.").length == 3 && !version.split("\\.")[2].equals(latestVer.split("\\.")[2])) {
                logger.severe("There is an minor update available at " +
                        releaseURL +"\n" + "The bot will still run but it is recommended to update!");
                logger.info("You are on release `" + version +"`. The latest release is " + latestVer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isLatestMajorVer) {
            DiscordApi DiscordApi = new DiscordApiBuilder().setToken(discordToken).login().join();

            HashMap<Long, Boolean> statuses = new HashMap<>();
            HashMap<Long, Long> roles = new HashMap<>();

            File ratBotFiles = new File("/Rat Bot Files");

            if (!ratBotFiles.exists()) {
                try {
                    boolean success = ratBotFiles.mkdir();
                    if (!success) {
                        throw new IOException("Could not create necessary files. Try running as administrator");
                    }
                    BufferedWriter statusesCreator = new BufferedWriter(new FileWriter("/Rat Bot Files/statuses.txt"));
                    statusesCreator.write("");
                    statusesCreator.close();
                    BufferedWriter rolesCreator = new BufferedWriter(new FileWriter("/Rat Bot Files/roles.txt"));
                    rolesCreator.write("");
                    rolesCreator.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                Scanner statusesScanner = new Scanner(new BufferedReader(new FileReader("/Rat Bot Files/statuses.txt")));
                while (statusesScanner.hasNextLine()) {
                    String line = statusesScanner.nextLine();
                    String[] keyValue = line.split(" ");
                    statuses.put(Long.parseLong(keyValue[0]), Boolean.parseBoolean(keyValue[1]));
                }
                Scanner rolesScanner = new Scanner(new BufferedReader((new FileReader("/Rat Bot Files/roles.txt"))));
                while (rolesScanner.hasNextLine()) {
                    String line = rolesScanner.nextLine();
                    String[] keyValue = line.split(" ");
                    roles.put(Long.parseLong(keyValue[0]), Long.parseLong(keyValue[1]));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            DiscordApi.addListener(new messageListener(DiscordApi.getCustomEmojis(), statuses, roles));
            DiscordApi.updateActivity(ActivityType.LISTENING, "!ratify");

            logger.info("Bot started!\n" +
                    "You can invite me to your server with " +
                    botHelper.botInviteMaker(singletonManager.getClientID(), singletonManager.getNeededPermissions()));
        } else {
            System.out.println("Press any key to exit");
            Scanner exit = new Scanner(System.in);
            exit.nextLine();
            exit.close();
        }
    }
}
