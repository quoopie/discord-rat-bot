package com.discord.bots;

import org.javacord.api.entity.permission.PermissionType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Hosts final variables that are needed across multiple classes
 */
public class SingletonManager {

    // Should not be called outside of class
    public SingletonManager() {}

    private static SingletonManager instance;

    /**
     * Variables used throughout bot that do not need to be changed
     */
    private final Logger logger = Logger.getLogger("SingletonManager");
    private final String clientID = "936771076288041001";
    private final List<PermissionType> neededPermissions = new ArrayList<>() {
        {
                add(PermissionType.ADD_REACTIONS);
                add(PermissionType.MANAGE_EMOJIS);
                add(PermissionType.READ_MESSAGES);
                add(PermissionType.READ_MESSAGE_HISTORY);
                add(PermissionType.SEND_MESSAGES);
                add(PermissionType.USE_EXTERNAL_EMOJIS);
                add(PermissionType.EMBED_LINKS);
        }
    };

    /**
     * @return Singleton instance of this class
     */
    public static SingletonManager getInstance() {
        if (instance == null) {
            instance = new SingletonManager();
        }
        return instance;
    }

    /**
     * @return The current logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * @return Bots needed Permissions in a collection
     */
    public Collection<PermissionType> getNeededPermissions() {
        return neededPermissions;
    }

    /**
     * @return Bots clientID
     */
    public String getClientID() {
        return clientID;
    }
}
