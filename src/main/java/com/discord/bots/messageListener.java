package com.discord.bots;

import org.javacord.api.entity.emoji.Emoji;
import org.javacord.api.entity.emoji.KnownCustomEmoji;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.entity.permission.Role;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

import java.io.*;
import java.util.*;

@SuppressWarnings("ClassCanBeRecord")
public class messageListener implements MessageCreateListener {

    private final Collection<KnownCustomEmoji> customEmojis;
    private final HashMap<Long, Boolean> statuses;
    private final HashMap<Long, Long> roles;

    public messageListener(Collection<KnownCustomEmoji> customEmojis, HashMap<Long, Boolean> statuses,
                           HashMap<Long, Long> roles) {
        this.customEmojis = customEmojis;
        this.statuses = statuses;
        this.roles = roles;
    }

    @SuppressWarnings("EnhancedSwitchMigration")
    @Override
    public void onMessageCreate(MessageCreateEvent event) {
        Message message = event.getMessage();
        Role role = null;

        if (message.getServer().isEmpty()) { throw new NullPointerException("Unreachable"); }
        final Long serverID = message.getServer().get().getId();
        if (roles.get(serverID) != null) {
            if (message.getServer().get().getRoleById(roles.get(serverID)).isPresent()) {
                role = message.getServer().get().getRoleById(roles.get(serverID)).get();
            }
        }

        File ratBotFiles = new File("/Rat Bot Files");

        String statusesPath = "/Rat Bot Files/statuses.txt";
        String rolesPath = "/Rat Bot Files/roles.txt";

        if (!ratBotFiles.exists()) {
            try {
                BufferedWriter statusesCreator = new BufferedWriter(new FileWriter(statusesPath));
                statusesCreator.write(serverID + " true");
                statusesCreator.close();
                BufferedWriter rolesCreator = new BufferedWriter(new FileWriter(rolesPath));
                rolesCreator.write(serverID + " null");
                rolesCreator.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        boolean containsEmoji;

        boolean statusWritten = false;
        boolean roleWritten = false;

        MessageBuilder noRoleSetError = new MessageBuilder()
                .append("Error! No role has been set for this server yet!")
                .appendNewLine()
                .append("Set your role with `!ratify setrole <roleID>`")
        ;



        switch (message.getContent().toLowerCase()) {
            case "!ratify knownemojis":
                if (message.getUserAuthor().isEmpty()) { throw new NullPointerException("Unreachable"); }
                if (role == null) {
                    noRoleSetError.send(message.getChannel());
                    return;
                }
                if (!role.hasUser(message.getUserAuthor().get())) {
                    message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Error! You cannot use this command!");
                    return;
                }
                StringBuilder finalMessage = new StringBuilder("I know");
                for (KnownCustomEmoji emoji : customEmojis) {
                    if (!message.getServer().get().getCustomEmojis().contains(emoji)) {
                        continue;
                    }
                    finalMessage.append(" ").append(emoji.getName());
                }
                message.getChannel().sendMessage(finalMessage.toString());
                break;
            case "!ratify enable":
                if (role == null) {
                    noRoleSetError.send(message.getChannel());
                    return;
                }
                if (!role.hasUser(message.getUserAuthor().get())) {
                    message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Error! You cannot use this command!");
                    return;
                }
                try {
                    statuses.put(serverID, true);
                    List<String> lines = new ArrayList<>();

                    Scanner statusScanner = new Scanner(new BufferedReader(new FileReader(statusesPath)));
                    while (statusScanner.hasNextLine()) {
                        lines.add(statusScanner.nextLine());
                    }
                    statusScanner.close();

                    BufferedWriter statusWriter = new BufferedWriter(new FileWriter(statusesPath));
                    for (String line : lines) {
                        if (line.contains(String.format("%d", serverID))) {
                            String[] valueKey = line.split(" ");
                            statusWriter.write(valueKey[0] + " true\n");
                            statusWritten = true;
                        } else { statusWriter.write(line + "\n"); }
                    }
                    if (!statusWritten) {
                        statusWriter.write(serverID + " true");
                    }
                    statusWriter.close();
                    message.getChannel().sendMessage( botHelper.messageAuthorPing(message) + " Rat Bot enabled :smiley:");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            break;
            case "!ratify disable":
                if (role == null) {
                    noRoleSetError.send(message.getChannel());
                    return;
                }
                if (!role.hasUser(message.getUserAuthor().get())) {
                    message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Error! You cannot use this command!");
                    return;
                }
                    try {
                        statuses.put(serverID, false);
                        List<String> lines = new ArrayList<>();

                        Scanner statusScanner = new Scanner(new BufferedReader(new FileReader(statusesPath)));
                        while (statusScanner.hasNextLine()) {
                            lines.add(statusScanner.nextLine());
                        }
                        statusScanner.close();

                        BufferedWriter statusWriter = new BufferedWriter(new FileWriter(statusesPath));
                        for (String line : lines) {
                            if (line.contains(String.format("%d", serverID))) {
                                String[] valueKey = line.split(" ");
                                statusWriter.write(valueKey[0] + " false\n");
                                statusWritten = true;
                            } else { statusWriter.write(line + "\n"); }
                        }
                        if (!statusWritten) {
                            statusWriter.write(serverID+ " false");
                        }
                        statusWriter.close();
                        message.getChannel().sendMessage( botHelper.messageAuthorPing(message) + " Rat Bot disabled :frowning2:");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
            break;
            case "!ratify":
                MessageBuilder infoMessage = new MessageBuilder()
                        .append(botHelper.messageAuthorPing(message) + " ")
                        .append("Available commands:")
                        .appendNewLine()
                        .append("`!ratify` - Brings up this message")
                        .appendNewLine()
                        .append("`!ratify enable` - Enables the bot on this server")
                        .appendNewLine()
                        .append("`!ratify disable` - Disables the bot on this server")
                        .appendNewLine()
                        .append("`!ratify knownemojis` - Shows what custom emojis the bot knows on this server,")
                        .appendNewLine()
                        .append("                                            which can be useful for debugging the bot on your server")
                        .appendNewLine()
                        .append("`!ratify setrole <roleid>` - Sets the role which has permission to enable or disable the bot")
                        .appendNewLine()
                        .append("`!ratify invite` - Sends invite link for the bot")
                        .appendNewLine()
                        .append("`!ratify gitlab` - Sends link to the bots gitlab")
                ;
                infoMessage.send(message.getChannel());
            break;
            case "!ratify invite":
                message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Invite me with:\n" + botHelper.botInviteMaker(SingletonManager.getInstance().getClientID(), SingletonManager.getInstance().getNeededPermissions()));
                break;
            case "!ratify gitlab":
                message.getChannel().sendMessage(botHelper.messageAuthorPing(message) +" https://gitlab.com/quoopie/discord-rat-bot");
                break;
        }

        if (message.getContent().startsWith("!ratify setrole")) {
            if (role != null) {
                if (!role.hasUser(message.getUserAuthor().get())) {
                    message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Error! You cannot use this command!");
                    return;
                }
            }
            String roleString = message.getContent().split(" ")[2];
            if (message.getServer().get().getRoleById(roleString).isEmpty()) {
                message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Error! Invalid role ID");
                return;
            }
            Role newRole = message.getServer().get().getRoleById(roleString).get();
            try {
                Scanner rolesReader = new Scanner(new BufferedReader(new FileReader("/Rat Bot Files/roles.txt")));
                List<String> lines = new ArrayList<>();
                while (rolesReader.hasNextLine()) {
                    String line = rolesReader.nextLine();
                    if (line.contains(String.format("%d", serverID))) {
                        lines.add(serverID + " " + newRole.getId());
                        roleWritten = true;
                    } else { lines.add(line); }
                }
                rolesReader.close();

                BufferedWriter rolesWriter = new BufferedWriter(new FileWriter("/Rat Bot Files/roles.txt"));
                for (String line : lines) { rolesWriter.write(line+"\n"); }
                if (!roleWritten) { rolesWriter.write(serverID + " " + newRole.getId()); }
                rolesWriter.close();

                roles.put(serverID, newRole.getId());

                role = newRole;

                message.getChannel().sendMessage(botHelper.messageAuthorPing(message) + " Role has been set to " + role.getMentionTag());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (statuses.containsKey(serverID)) {
            if (statuses.get(serverID)) {
                for (KnownCustomEmoji emoji : customEmojis) {
                    if (!message.getServer().get().getCustomEmojis().contains(emoji)) {
                        continue;
                    }
                    containsEmoji = checkReactions(emoji, message);
                    if (emoji.getName().equals("funny_rat") && !containsEmoji) {
                        message.addReaction(emoji.getReactionTag());
                    }
                }
            }
        } else {
            try {
                statuses.put(serverID, true);
                List<String> lines = new ArrayList<>();

                Scanner statusScanner = new Scanner(new BufferedReader(new FileReader(statusesPath)));
                while (statusScanner.hasNextLine()) {
                    lines.add(statusScanner.nextLine());
                }
                statusScanner.close();

                BufferedWriter statusWriter = new BufferedWriter(new FileWriter(statusesPath));
                for (String line : lines) {
                    statusWriter.write(line + "\n");
                }
                statusWriter.write(serverID + " true");
                statusWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean checkReactions(KnownCustomEmoji emoji, Message message) {
        for (Emoji existingEmoji : message.getCustomEmojis()) {
            KnownCustomEmoji existingEmojiAsKnownCustomEmoji = null;
            if (existingEmoji.isKnownCustomEmoji()) {
                if (existingEmoji.asKnownCustomEmoji().isEmpty()) { throw new NullPointerException("Unreachable"); }
                existingEmojiAsKnownCustomEmoji = existingEmoji.asKnownCustomEmoji().get();
            }
            if (existingEmojiAsKnownCustomEmoji != null && existingEmojiAsKnownCustomEmoji.getName().equalsIgnoreCase(emoji.getName())) { return true; }
        }
        return false;
    }
}
